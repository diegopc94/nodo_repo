terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-west-2"
}

resource "aws_subnet" "vpc_1" {
  vpc_id            = aws_vpc.vpc_1.id
  cidr_block        = "11.22.33.0/16"
  availability_zone = "us-west-2a"

  tags = {
    Name = "vpc_1"
  }
}

resource "aws_network_interface" "subnet_1" {
  subnet_id   = aws_subnet.subnet_1.id
  private_ips = ["11.22.33.44"]

  tags = {
    Name = "subnet_1"
  }
}


resource "aws_instance" "t3a-nano" {
  ami           = "ami-830c94e3"
  instance_type = "t3a-nano"
  key_name = "${var.ami_key_pair_name}"
  security_groups = ["${aws_security_group.ingress-all-test.id}"]
  tags = {
    Name = "Amazon-Linux-2"
  }
}


