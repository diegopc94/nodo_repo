FROM node:alpine
RUN mkdir -p /app
WORKDIR /app
COPY package*.json index.js /app/
RUN npm install -g npm@8.5.5
#RUN npm i @koa/router
RUN npm install -g koa@2.1.0
COPY . /app/
EXPOSE 8181
CMD ["npm", "run", "start"]
